import React from 'react'
import {Container, Table} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';


function TableAccount({itemtable}) {
    
    return (
        <Container style={{width:'100%', margin:'250px 100px'}}>
            <h1>Table Accounts</h1>
            <Table striped bordered hover>
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Password</th>
                    <th>Gender</th>
                    </tr>
                </thead>
                {
                    itemtable.map((item, inx)=>(
                            <tbody>
                            <tr >
                            <td></td>
                            <td></td>
                            <td>{item. username}</td>
                            <td>{item.email}</td>
                            <td>{item.password}</td>
                            </tr>
                            </tbody>
                        
                    ))
                }
                 {/* <tbody>
                            <tr >
                            <td></td>
                            <td></td>
                            <td>username</td>
                            <td>email</td>
                            <td>password</td>
                            </tr>
                            </tbody>
                 */}
            
            </Table>

        </Container>
    )
}
export default TableAccount
