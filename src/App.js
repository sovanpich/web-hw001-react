
import './App.css';
import { Container,Row,Col,Form,Image,Button } from 'react-bootstrap';
import TableAccount from './Components/TableAccount';
import React, { Component } from 'react'

export default class App extends Component {
  constructor(props) {
    super(props)

    this.state ={
      data:[{
        id:'',
        username:'',
        gender:'',
        email:'',
        password:'',
        onChangeEmail:this.onChangeUsename
      }]
    };

  }
  
 onChangeUsename= event =>{
     this.setState({
         username:event.target.value
     })
 }
 onChangeEmail= event =>{
  this.setState({
      email:event.target.value
  })
}
onChangePassword= event =>{
  this.setState({
      password:event.target.value
  })
}
handleClick=()=>{
  alert(`${this.state.username} ${this.state.email} ${this.state.password}`)
}
  render() {
    return (
      <Container>
      <Row>
          <Col>
          <form style={{width:'100%', margin:'120px 50px'}}>
          <Image style={{marginLeft:'30px'}} src="account.png" roundedCircle />
          <h1 style={{margin:'0px'}}>Creact Account</h1>
          <Form.Label>Username</Form.Label>
          <Form.Control type="text" placeholder="username" value={this.state.username} onChange={this.onChangeUsename} />
          <Form.Label>Gender</Form.Label>
          <br/>
          <Form.Check style={{display:'inline'}} type="radio" />
          <Form.Label style={{ marginLeft:'10px'}} >Male</Form.Label>
          <Form.Check style={{display:'inline',marginLeft:'10px'}} type="radio" />
          <Form.Label style={{ marginLeft:'10px'}} >Female</Form.Label>
          <br/>
          <Form.Label>Email</Form.Label>
          <Form.Control type="email" placeholder="email.com" value={this.state.email} onChange={this.onChangeEmail} />
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="password" value={this.state.password} onChange={this.onChangePassword} />
          <Button style={{marginTop:'12px'}} onClick={this.handleClick}>Save</Button>
      </form>
            </Col>
            <Col>
            <TableAccount itemtable={this.state.data}/>
            </Col>
          </Row>
    </Container>
    )
  }
}

